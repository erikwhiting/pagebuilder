import json
import xml.etree.ElementTree as et


def extract_data(file_location):
    page_data_object = None
    k = file_location.rfind('.')
    extension = file_location[k + 1:]
    if extension == 'json':
        page_data_object = page_data_from_json(file_location)
    elif extension == 'xml':
        page_data_object = page_data_from_xml(file_location)

    return page_data_object


def page_data_from_json(json_file_location):
    with open(json_file_location) as json_file:
        json_data = json.load(json_file)

    page_name = json_data.get('page name')
    page_url = json_data.get('url')
    elements_array = json_data.get('elements')

    element_data_objects = []
    for element in elements_array:
        element_data_object = {
            'name': element.get('name'),
            'value': element.get('value'),
            'indicator': element.get('indicator')
        }
        element_data_objects.append(element_data_object)

    page_data_object = {
        'page_name': page_name,
        'page_url': page_url,
        'elements': element_data_objects
    }

    return page_data_object


def page_data_from_xml(xml_file_location):
    xml_data = et.parse(xml_file_location).getroot()
    page_name = xml_data.find('page_name').text
    page_url = xml_data.find('url').text
    elements_array = xml_data.findall('elements/element')

    element_data_objects = []
    for element in elements_array:
        element_data_object = {
            'name': element.find('name').text,
            'value': element.find('value').text,
            'indicator': element.find('indicator').text
        }
        element_data_objects.append(element_data_object)

    page_data_object = {
        'page_name': page_name,
        'page_url': page_url,
        'elements': element_data_objects
    }

    return page_data_object


def page_data_from_database(page_id):
    # TODO: write this function, but for now, pay attention to what you were doing in the first place

    return None

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class BaseElement(object):
    def __init__(self, driver, name, value, by):
        self.name = name
        self.driver = driver
        self.value = value
        self.by = by
        self.locator = (self.by, self.value)
        self.web_element = self.element()

    def element(self):
        element = WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located(locator=self.locator))
        return element

    def click(self):
        self.web_element.click()

    def input_text(self, text):
        self.web_element.send_keys(text)

    def clear(self):
        self.web_element.clear()

    def clear_text(self):
        self.web_element.send_keys(Keys.CONTROL + 'a')
        self.web_element.send_keys(Keys.DELETE)

    def drop_down_select(self, index_of_item_to_select):
        self.web_element.select_by_index(index_of_item_to_select)

    @property
    def element_text(self):
        return self.web_element.text

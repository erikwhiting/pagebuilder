from selenium.webdriver.common.by import By
from depot.base_element import BaseElement


def load_element(driver, element_name, value, indicator='id'):
    indicator = indicator.lower()
    indicator_converter = {
        "id": By.ID,
        "xpath": By.XPATH,
        "selector": By.CSS_SELECTOR,
        "class": By.CLASS_NAME,
        "link text": By.LINK_TEXT,
        "name": By.NAME,
        "partial link": By.PARTIAL_LINK_TEXT,
        "tag": By.TAG_NAME
    }

    return BaseElement(driver, element_name, by=indicator_converter.get(indicator), value=value)


from depot.element_utils import load_element
import importlib


class BasePage:

    name = ''
    url = ''
    page_elements = []
    page_data = {}

    def __init__(self, driver, page_data_source):
        self.driver = driver
        self.page_data_source = page_data_source
        self.load_page()

    def load_page(self):
        extractor = importlib.import_module('depot.extract_page_data')
        self.page_data = extractor.extract_data(self.page_data_source)
        self.name = self.page_data.get('page_name')
        self.url = self.page_data.get('page_url')

    def go(self):
        self.driver.get(self.url)
        # Elements cannot be built until page is loaded in browser
        elements = self.page_data.get('elements')
        for data_element in elements:
            self.add_new_element(data_element.get('name'), data_element.get('value'), data_element.get('indicator'))

    def add_new_element(self, element_name, by_value, indicator='id'):
        element = load_element(self.driver, element_name, by_value, indicator)
        self.page_elements.append(element)

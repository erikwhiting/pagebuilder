import configparser
import os


class Config(object):
    def __init__(self):
        # Using the fully qualified path so that the unit test will work
        page_builder_root = os.environ['PAGE_BUILDER_ROOT']
        self.config_file = page_builder_root + '\\configurations\\config.ini'
        cf = configparser.ConfigParser()
        cf.read(self.config_file)

        self.preferred_file_type = cf['Files']['preferred_file_type']
        self.save_path = cf['Environments']['local']
        self.json_path = self.save_path + cf['Paths']['json']
        self.xml_path = self.save_path + cf['Paths']['xml']
        self.test_json_path = self.save_path + cf['Paths']['test_json']
        self.test_xml_path = self.save_path + cf['Paths']['test_xml']
        remove_spaces = cf['Files']['replace_file_name_spaces']
        if remove_spaces.lower() == 'true' or remove_spaces.lower() == 'yes' or remove_spaces == 1:
            self.remove_file_spaces = True
        else:
            self.remove_file_spaces = False




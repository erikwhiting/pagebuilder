import os
from configurations import config


def save(file_object, file_type, file_name, test_mode=False):
    resolved_file_name = set_file_name(file_type, file_name, test_mode)
    resolved_file_path = set_file_path(file_type, test_mode)
    fully_qualified_file_name = resolved_file_path + resolved_file_name
    file = open(fully_qualified_file_name, 'w+')
    file.write(file_object)
    file.close()


def set_file_name(file_type, file_name, test_mode):
    cf = config.Config()
    replace_spaces = cf.remove_file_spaces
    if replace_spaces:
        file_name = file_name.replace(' ', '_')

    suffix = '.' + file_type
    file_path = set_file_path(file_type, test_mode)
    resolved_file_name = file_name
    resolved_file_name += set_revision_number(file_name, file_path)
    resolved_file_name += suffix
    return resolved_file_name


def set_file_path(file_type, test_mode):
    cf = config.Config()
    file_path = ''

    if test_mode:
        if file_type == 'json':
            file_path = cf.test_json_path
        elif file_type == 'xml':
            file_path = cf.test_xml_path
    else:
        if file_type == 'json':
            file_path = cf.json_path
        elif file_type == 'xml':
            file_path = cf.xml_path

    file_path += '/'

    return file_path


def set_revision_number(file_name, file_path):
    target_folder_files = os.listdir(file_path)
    revision_number = 1
    for file in target_folder_files:
        k = file.rfind('_')
        file_without_revision_number = file[:k]
        current_revision = int(file[k+2:file.rfind('.')])
        if file_name == file_without_revision_number:
            revision_number = current_revision + 1

    resolved_revision_number = '_v' + str(revision_number)
    return resolved_revision_number

import unittest
import os
import shutil

from files import save_file
from configurations import config


class TestFiles(unittest.TestCase):

    def setUp(self):
        self.cf = config.Config()
        os.mkdir(self.cf.test_json_path)
        os.mkdir(self.cf.test_xml_path)

    def tearDown(self):
        shutil.rmtree(self.cf.test_json_path)
        shutil.rmtree(self.cf.test_xml_path)

    def test_save_new_json_file(self):
        file_location = './test_json.json'
        file_name = 'unit_test_file_json'
        json_file = open(file_location, 'r')
        json_object = json_file.read()
        save_file.save(json_object, 'json', file_name, test_mode=True)
        json_test_directory = self.cf.test_json_path
        directory_contents = os.listdir(json_test_directory)
        self.assertIn(file_name + '_v1.json', directory_contents)

    def test_save_new_xml_file(self):
        file_location = './test_xml.xml'
        file_name = 'unit_test_file_xml'
        xml_file = open(file_location, 'r')
        xml_object = xml_file.read()
        save_file.save(xml_object, 'xml', file_name, test_mode=True)
        xml_test_directory = self.cf.test_xml_path
        directory_contents = os.listdir(xml_test_directory)
        self.assertIn(file_name + '_v1.xml', directory_contents)

    def test_save_version_json_file(self):
        file_name = 'version_json_test'
        save_file.save(' ', 'json', file_name, test_mode=True)
        save_file.save(' ', 'json', file_name, test_mode=True)
        json_test_directory = self.cf.test_json_path
        directory_contents = os.listdir(json_test_directory)
        self.assertIn(file_name + '_v2.json', directory_contents)

    def test_save_version_xml_file(self):
        file_name = 'version_xml_test'
        save_file.save(' ', 'xml', file_name, test_mode=True)
        save_file.save(' ', 'xml', file_name, test_mode=True)
        xml_test_directory = self.cf.test_xml_path
        directory_contents = os.listdir(xml_test_directory)
        self.assertIn(file_name + '_v2.xml', directory_contents)


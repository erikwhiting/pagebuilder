from selenium import webdriver
from depot.base_page import BasePage
import unittest


class TestLoaders(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.quit()

    def test_json_loader(self):

        file_location = './test_json.json'
        target_url = 'blog.erikwhiting.com'
        tp = BasePage(self.browser, file_location)
        tp.go()
        tp.page_elements[0].click()
        actual_url = self.browser.current_url
        self.assertIn(target_url, actual_url, 'Clicking Blog Link failed')

    def test_xml_loader(self):

        file_location = './test_xml.xml'
        target_url = 'blog.erikwhiting.com'
        tp = BasePage(self.browser, file_location)
        tp.go()
        tp.page_elements[0].click()
        actual_url = self.browser.current_url
        self.assertIn(target_url, actual_url, 'Clicking Blog Link failed')




